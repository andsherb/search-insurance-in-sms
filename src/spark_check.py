from pyspark.sql import SparkSession
from pyspark.sql.types import StructType
import pyspark.sql.functions as fn


def _initialize_spark() -> SparkSession:
    """Create a Spark Session"""

    name = 'Insurance'
    spark = SparkSession.builder \
        .appName(name) \
        .master('local[*]') \
        .getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    return spark




if __name__ == '__main__':
    spark = _initialize_spark()
    print(spark.version)
    # spark.read.parquet(
    #     "../balance_search/data/raw/test.parquet"
    # ).limit(
    #     5000000
    # ).repartition(
    #     200
    # ).write.mode(
    #     "overwrite"
    # ).parquet(
    #     "../balance_search/data/raw/test"
    # )
    schema = StructType() \
        .add('text', 'string') \
        # .add('msisdn', 'integer')
    initDF = spark \
        .readStream \
        .format("parquet") \
        .option("path", '../balance_search/data/raw/test/') \
        .option("maxFilesPerTrigger", 1) \
        .schema(schema) \
        .load()
    resultDF = initDF.select(
        "text"
    ).withColumn(
        "text", fn.lower(fn.col("text"))
    ).filter(
        fn.col("text").contains("баланс")
    )
    query = resultDF.writeStream \
        .outputMode("append") \
        .format("console") \
        .option("truncate", "false") \
        .start()
    #
    query.awaitTermination()

    spark.stop()
