from pyspark.sql import SparkSession
from pyspark.ml.feature import  Tokenizer, NGram, CountVectorizer, VectorAssembler
from pyspark.ml import Pipeline
from pyspark.sql.functions import split, col, concat_ws, concat, array_except, when
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.evaluation import BinaryClassificationEvaluator, MulticlassClassificationEvaluator
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
import numpy as np
import mlflow.spark
import mlflow

from datetime import datetime
import click
import logging
from pathlib import Path


mlflow.set_tracking_uri("http://0.0.0.0:5000")
mlflow.set_experiment('pyspark')


def _initialize_spark() -> SparkSession:
    """Create a Spark Session"""

    name = 'Anonymizer for sms texts'
    spark = SparkSession.builder \
        .appName(name) \
        .config("spark.jars.packages", "ai.catboost:catboost-spark_3.1_2.12:1.0.6") \
        .getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    print(get_time(), 'Initialize spark')

    return spark


def get_time():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '@@@ [insurance]'


def build_ngrams(inputCol="tokens", k=1, n=3):
    ngrams = [
        NGram(n=i, inputCol="tokens", outputCol="{0}_grams".format(i))
        for i in range(k, n + 1)
    ]
    return Pipeline(stages=ngrams)

# def build_ngrams(inputCol="tokens", k=1, n=3):
#
#     ngrams = [
#         NGram(n=i, inputCol="tokens", outputCol="{0}_grams".format(i))
#         for i in range(k, n + 1)
#     ]
#
#     vectorizers = [
#         CountVectorizer(inputCol="{0}_grams_1".format(i),
#             outputCol="{0}_counts".format(i))
#         for i in range(k, n + 1)
#     ]
#
#     assembler = [VectorAssembler(
#         inputCols=["{0}_counts".format(i) for i in range(k, n + 1)],
#         outputCol="features"
#     )]
#
#     return Pipeline(stages=ngrams + vectorizers + assembler)



@click.command()
@click.argument("input_data_path", type=click.Path(exists=True))
def train_model(
        input_data_path: str
):
    print(get_time(), 'Main started')
    spark = _initialize_spark()
    import catboost_spark

    df = spark.read.csv(input_data_path, inferSchema=True, header=True)
    df = df.withColumn('tokens', split(col('text'), ''))

    seed = 42  # set seed for reproducibility
    # ngrams range
    k = 3  # min ngram
    n = 5  # max ngram

    trainDF, testDF = df.randomSplit([0.8, 0.2], seed)
    ngrams_builder = build_ngrams(k=k, n=n).fit(trainDF)
    trainDF = ngrams_builder.transform(trainDF)
    testDF = ngrams_builder.transform(testDF)

    trainDF = trainDF.withColumn('features', concat(*[col("{0}_grams".format(i)) for i in range(k, n + 1)]))
    testDF = testDF.withColumn('features', concat(*[col("{0}_grams".format(i)) for i in range(k, n + 1)]))

    count = CountVectorizer(inputCol="features", outputCol="rawFeatures", maxDF=0.8, minDF=3)
    model = count.fit(trainDF)
    trainDF = model.transform(trainDF).select('target', 'rawFeatures')
    testDF = model.transform(testDF).select('target', 'rawFeatures')

    TARGET_LABEL = 'target'
    FEATURES = 'rawFeatures'
    train_pool = catboost_spark.Pool(trainDF)
    train_pool.setLabelCol(TARGET_LABEL)
    train_pool.setFeaturesCol(FEATURES)

    evaluator = MulticlassClassificationEvaluator(
        labelCol=TARGET_LABEL,
        predictionCol=FEATURES,
        metricName='accuracy')

    # lr = LogisticRegression(featuresCol='rawFeatures', labelCol='target')
    #
    # paramGrid_lr = ParamGridBuilder() \
    #     .addGrid(lr.regParam, [0.5, 0.6]) \
    #     .addGrid(lr.elasticNetParam, [0.02, 0.03]) \
    #     .build()

    clf = catboost_spark.CatBoostClassifier(featuresCol=FEATURES, labelCol=TARGET_LABEL)
    clf.setIterations(10)
    clf.setDepth(5)
    # clf.setLearningRate(0.75)
    # clf.setL2LeafReg(2)
    # clf.setBorderCount(32)

    model = clf.fit(train_pool)
    # predict = model.transform(testDF)
    # print(f'Model F1 = {evaluator.evaluate(predict)}')



    # with mlflow.start_run(run_name='CatBoost'):
    #     clf.fit(train_pool)
    #     eval = MulticlassClassificationEvaluator(labelCol='target', metricName='accuracy')
    #
    # # print(f'C={cvModel_lr.bestModel.getRegParam()}')
    # # print(f'ElasticNet={cvModel_lr.bestModel.getElasticNetParam()}')
    #
    #     predictions_train = clf.transform(testDF)
    #     test_metric = eval.evaluate(clf.transform(testDF))
    #
    #     # Log best model metric
    #     mlflow.log_metric('test_' + eval.getMetricName(), test_metric)
    #
    #     # Log the best model.
    #     mlflow.spark.log_model(spark_model=clf, artifact_path='best-model')



if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    train_model()
