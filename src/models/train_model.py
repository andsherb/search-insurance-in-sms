import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
import re
from nltk.tokenize import RegexpTokenizer
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, f1_score, precision_score,\
    recall_score, confusion_matrix, roc_auc_score
from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split
import matplotlib
import matplotlib.patches as mpatches
import joblib
import os
import itertools

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import GridSearchCV, StratifiedKFold, RandomizedSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import CountVectorizer
import click
import mlflow
import warnings
import tempfile
import os
from datetime import datetime
import logging
from pathlib import Path
from catboost import CatBoostClassifier, Pool



def log_run(gridsearch, experiment_name: str, model_name: str, run_index: int, conda_env,
            tags={}):
    """Logging of cross validation results to mlflow tracking server

    Args:
        experiment_name (str): experiment name
        model_name (str): Name of the model
        run_index (int): Index of the run (in Gridsearch)
        conda_env (str): A dictionary that describes the conda environment (MLFlow Format)
        tags (dict): Dictionary of extra data and tags (usually features)
    """

    cv_results = gridsearch.cv_results_
    # print(cv_results)
    with mlflow.start_run(run_name=str(run_index)) as run:

        mlflow.log_param("folds", gridsearch.cv)

        print("Logging parameters")

        try:
            params = list(gridsearch.param_grid.keys())
        except AttributeError:
            params = list(gridsearch.param_distributions.keys())

        for param in params:
            mlflow.log_param(param, cv_results["param_%s" % param][run_index])
            print("param_%s" % param)

        print("Logging metrics")
        for score_name in [score for score in cv_results if "mean_test" in score]:
            # print(score_name)
            mlflow.log_metric(score_name, cv_results[score_name][run_index])
            mlflow.log_metric(score_name.replace("mean", "std"),
                              cv_results[score_name.replace("mean", "std")][run_index])

        print("Logging model")
        mlflow.sklearn.log_model(gridsearch.best_estimator_, model_name, conda_env=conda_env)

        print("Logging CV results matrix")
        tempdir = tempfile.TemporaryDirectory().name
        os.mkdir(tempdir)
        timestamp = datetime.now().isoformat().split(".")[0].replace(":", ".")
        filename = "%s-%s-cv_results.csv" % (model_name, timestamp)
        csv = os.path.join(tempdir, filename)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            pd.DataFrame(cv_results).to_csv(csv, index=False)


        mlflow.log_artifact(csv, "cv_results")

        print("Logging extra data related to the experiment")
        mlflow.set_tags(tags)

        run_id = run.info.run_uuid
        experiment_id = run.info.experiment_id
        print(mlflow.get_artifact_uri())
        print("runID: %s" % run_id)
        mlflow.end_run()


def log_results(gridsearch: GridSearchCV, experiment_name, model_name, tags={}, log_only_best=False):
    """Logging of cross validation results to mlflow tracking server

    Args:
        experiment_name (str): experiment name
        model_name (str): Name of the model
        tags (dict): Dictionary of extra tags
        log_only_best (bool): Whether to log only the best model in the gridsearch or all the other models as well
    """
    conda_env = {
        'name': 'mlflow-env',
        'channels': ['defaults'],
        'dependencies': [
            'python=3.8.0',
            'scikit-learn>=0.21.3',
            {'pip': ['xgboost==1.0.1']}
        ]
    }

    best = gridsearch.best_index_

    mlflow.set_tracking_uri("http://0.0.0.0:5000")
    mlflow.set_experiment(experiment_name)

    if (log_only_best):
        log_run(gridsearch, experiment_name, model_name, best, conda_env, tags)
    else:
        for i in range(len(gridsearch.cv_results_['params'])):
            log_run(gridsearch, experiment_name, model_name, i, conda_env, tags)


@click.command()
@click.argument("input_data_path", type=click.Path(exists=True))
@click.argument("n_iter", type=int)
@click.argument("best", type=bool)
# @click.argument("output_model_path", type=click.Path())
# @click.argument("output_train_metrics_path", type=click.Path())
def train_model(
        input_data_path: str,
        n_iter: int,
        best: bool,
        # output_model_path: str, output_train_metrics_path: str
):
    data = pd.read_csv(input_data_path)
    X = data["text"]
    y = data["target"]

    cv = StratifiedKFold(shuffle=True, random_state=42)

    logreg = Pipeline([
        ('vect', CountVectorizer(analyzer='char')),
        # ('tfidf', TfidfTransformer()),
        ('clf', LogisticRegression(n_jobs=-1,
                                   multi_class='auto',
                                   max_iter=5000,
                                   random_state=42)),
    ])

    params = {'vect__ngram_range': [(1, 2), (1, 3), (1, 4), (1, 5), (2, 3), (2, 4), (2, 5), (2, 6),
                                    (2, 7), (2, 8), (3, 4), (3, 5), (3, 6), (3, 8), (3, 10), (5, 6),
                                    (5, 7), (5, 9), (5, 11)],
              'vect__max_df': [0.1, 0.2, 0.4, 0.6, 0.7, 0.8, 0.9, 1.0],
              'vect__min_df': [1, 2, 3, 4, 5],
              'clf__C': np.logspace(-0.3, 0, 20),
              'clf__solver': ['lbfgs', 'newton-cg']
              }

    r_search = RandomizedSearchCV(estimator=logreg,
                                  param_distributions=params,
                                  n_iter=n_iter,
                                  scoring='accuracy',
                                  cv=cv,
                                  verbose=5,
                                  n_jobs=-1)

    r_search.fit(X=X, y=y)
    print(r_search.best_params_)
    print(r_search.best_score_)
    log_results(r_search,
                'test',
                'log_reg_vect',
                log_only_best=best,
                tags={'tfidf_transf': 'no',
                      'tfidf_vect': 'no',
                      'cnt_vect': 'yes'})


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    train_model()





