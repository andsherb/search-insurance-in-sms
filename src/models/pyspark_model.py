from pyspark.sql import SparkSession
from pyspark.ml.feature import NGram, CountVectorizer
from pyspark.ml import Pipeline
from pyspark.sql.functions import split, col, concat
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from pyspark.ml.tuning import CrossValidator, ParamGridBuilder
import mlflow.spark
import mlflow

from datetime import datetime
import click
import logging
from pathlib import Path


mlflow.set_tracking_uri("http://0.0.0.0:5000")
mlflow.set_experiment('pyspark')


def _initialize_spark() -> SparkSession:
    """Create a Spark Session"""

    name = 'Insurance'
    spark = SparkSession.builder \
        .appName(name) \
        .master('local[*]') \
        .getOrCreate()
    spark.sparkContext.setLogLevel("WARN")
    print(get_time(), 'Initialize spark')

    return spark


def get_time():
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S') + '@@@ [insurance]'


def build_ngrams(inputcol="tokens", k=1, n=3):
    ngrams = [
        NGram(n=i, inputCol=inputcol, outputCol="{0}_grams".format(i))
        for i in range(k, n + 1)
    ]
    return Pipeline(stages=ngrams)

# def build_ngrams(inputCol="tokens", k=1, n=3):
#
#     ngrams = [
#         NGram(n=i, inputCol="tokens", outputCol="{0}_grams".format(i))
#         for i in range(k, n + 1)
#     ]
#
#     vectorizers = [
#         CountVectorizer(inputCol="{0}_grams_1".format(i),
#             outputCol="{0}_counts".format(i))
#         for i in range(k, n + 1)
#     ]
#
#     assembler = [VectorAssembler(
#         inputCols=["{0}_counts".format(i) for i in range(k, n + 1)],
#         outputCol="features"
#     )]
#
#     return Pipeline(stages=ngrams + vectorizers + assembler)


@click.command()
@click.argument("input_data_path", type=click.Path(exists=True))
def train_model(
        input_data_path: str
):
    print(get_time(), 'Main started')
    spark = _initialize_spark()

    df = spark.read.csv(input_data_path, inferSchema=True, header=True)
    df = df.withColumn('tokens', split(col('text'), ''))

    seed = 42  # set seed for reproducibility
    # ngrams range
    k = 3  # min ngram
    n = 5  # max ngram

    TARGET_LABEL = 'target'
    FEATURES = 'rawFeatures'

    # trainDF, testDF = df.randomSplit([0.8, 0.2], seed)
    trainDF = df
    testDF = df
    ngrams_builder = build_ngrams(k=k, n=n).fit(trainDF)
    ngrams_builder.write().overwrite().save('models/ngrams_builder')
    trainDF = ngrams_builder.transform(trainDF)
    testDF = ngrams_builder.transform(testDF)

    trainDF = trainDF.withColumn('features', concat(*[col("{0}_grams".format(i)) for i in range(k, n + 1)]))
    testDF = testDF.withColumn('features', concat(*[col("{0}_grams".format(i)) for i in range(k, n + 1)]))

    count = CountVectorizer(inputCol="features", outputCol=FEATURES, maxDF=0.8, minDF=3)
    model = count.fit(trainDF)
    model.write().overwrite().save('models/count_vectorizer')
    trainDF = model.transform(trainDF).select(TARGET_LABEL, FEATURES)
    testDF = model.transform(testDF).select(TARGET_LABEL, FEATURES)

    lr = LogisticRegression(featuresCol=FEATURES, labelCol=TARGET_LABEL)

    paramGrid_lr = ParamGridBuilder() \
        .addGrid(lr.regParam, [0.5]) \
        .addGrid(lr.elasticNetParam, [0.02]) \
        .build()

    crossval_lr = CrossValidator(estimator=lr,
                                 estimatorParamMaps=paramGrid_lr,
                                 evaluator=MulticlassClassificationEvaluator(labelCol=TARGET_LABEL,
                                                                             metricName='accuracy'),
                                 numFolds=5)

    with mlflow.start_run(run_name='LogReg'):
        cvModel_lr = crossval_lr.fit(trainDF)
        eval = MulticlassClassificationEvaluator(labelCol=TARGET_LABEL, metricName='accuracy')

    # print(f'C={cvModel_lr.bestModel.getRegParam()}')
    # print(f'ElasticNet={cvModel_lr.bestModel.getElasticNetParam()}')

        predictions_train = cvModel_lr.transform(testDF)
        test_metric = eval.evaluate(cvModel_lr.transform(testDF))
        cvModel_lr.write().overwrite().save('models/classifier')

        # Log best model metric
        mlflow.log_metric('test_' + eval.getMetricName(), test_metric)

        # Log the best model.
        mlflow.spark.log_model(spark_model=cvModel_lr.bestModel, artifact_path='best-model')

        # Log best parameters
        for i in paramGrid_lr[0].keys():
            name = i.name
            param_value = getattr(cvModel_lr.bestModel, f'get{name[0].upper() + name[1:]}')
            try:
                mlflow.log_param(name, param_value())
            except TypeError:
                mlflow.log_param(name, param_value)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    train_model()
