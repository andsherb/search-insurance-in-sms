import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import random
import re
from nltk.tokenize import RegexpTokenizer
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score, f1_score, precision_score, \
    recall_score, confusion_matrix, roc_auc_score
from sklearn.metrics import check_scoring

from sklearn.decomposition import PCA, TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import train_test_split
import matplotlib
import matplotlib.patches as mpatches
import joblib
import os
import itertools

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import StratifiedKFold, ParameterSampler, KFold
from sklearn.metrics import check_scoring, accuracy_score
from sklearn.base import clone

import click
import mlflow
import warnings
import tempfile
import os
from datetime import datetime
import logging
from pathlib import Path
from catboost import CatBoostClassifier, Pool
from itertools import product


class GS_catboost():
    def __init__(
            self,
            estimator,
            param_grid,
            n_iter,
            cv=kFold(),
            scoring=None):
        self.estimator = estimator
        self.param_grid = param_grid
        self.n_iter = n_iter
        if cv:
            self.cv = cv
        else:
            self.cv = KFold(random_state=42)
        if scoring:
            self.scoring = check_scoring(self.estimator, scoring=scoring)
        else:
            self.scoring = check_scoring(self.estimator, scoring='accuracy')
        self.cv_results_ = {'params': [], 'mean_test_score': [], 'std_test_score': [], 'index': []}
        self.best_score_ = 0
        self.best_index_ = 0
        self.best_params_ = {}
        self.best_estimator_ = None

    def score(self, X, y=None):
        return self.scoring(self.best_estimator_, X, y)

    def predict(self, X):
        return self.best_estimator_.predict(X)

    def fit(self, X, y):
        grid = ParameterSampler(self.param_grid, n_iter=self.n_iter, random_state=42)
        for i, settings in enumerate(grid):
            scores = []
            base_estimator = clone(self.estimator)
            base_estimator.set_params(**settings)
            for train_index, test_index in self.cv.split(X, y):
                X_train, X_test = X.iloc[train_index], X.iloc[test_index]
                y_train, y_test = y.iloc[train_index], y.iloc[test_index]



                train_pool = Pool(data=pd.DataFrame(X_train), label=y_train,
                                  text_features=['text'])
                valid_pool = Pool(data=pd.DataFrame(X_test), label=y_test,
                                  text_features=['text'])

                base_estimator.fit(train_pool, eval_set=valid_pool, plot=False)
                score = self.scoring(base_estimator, valid_pool, y_test)
                scores.append(score)
            avg_score = np.mean(scores)
            std_score = np.std(scores)
            self.cv_results_['params'].append(settings)
            self.cv_results_['mean_test_score'].append(avg_score)
            self.cv_results_['std_test_score'].append(std_score)
            self.cv_results_['index'].append(i)
            if avg_score > self.best_score_:
                self.best_score_ = avg_score
                self.best_params_ = settings
                self.best_estimator_ = base_estimator
                self.best_index_ = i
        return self


def log_run(gridsearch, experiment_name: str, model_name: str, run_index: int, conda_env,
            tags=None):
    """Logging of cross validation results to mlflow tracking server

    Args:
        experiment_name (str): experiment name
        model_name (str): Name of the model
        run_index (int): Index of the run (in Gridsearch)
        conda_env (str): A dictionary that describes the conda environment (MLFlow Format)
        tags (dict): Dictionary of extra data and tags (usually features)
    """

    if tags is None:
        tags = {}
    cv_results = gridsearch.cv_results_
    # print(cv_results)
    with mlflow.start_run(run_name=str(run_index)) as run:

        mlflow.log_param("folds", gridsearch.cv)

        print("Logging parameters")

        try:
            params = list(gridsearch.param_grid.keys())
        except AttributeError:
            params = list(gridsearch.param_distributions.keys())

        for param in params:
            mlflow.log_param(param, cv_results['params'][run_index][param])

        print("Logging metrics")
        for score_name in [score for score in cv_results if "mean_test" in score]:
            # print(score_name)
            mlflow.log_metric(score_name, cv_results[score_name][run_index])
            mlflow.log_metric(score_name.replace("mean", "std"),
                              cv_results[score_name.replace("mean", "std")][run_index])

        print("Logging model")
        mlflow.catboost.log_model(gridsearch.best_estimator_, model_name, conda_env=conda_env)

        print("Logging CV results matrix")
        tempdir = tempfile.TemporaryDirectory().name
        os.mkdir(tempdir)
        timestamp = datetime.now().isoformat().split(".")[0].replace(":", ".")
        filename = "%s-%s-cv_results.csv" % (model_name, timestamp)
        csv = os.path.join(tempdir, filename)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            pd.DataFrame(cv_results).to_csv(csv, index=False)

        mlflow.log_artifact(csv, "cv_results")

        print("Logging extra data related to the experiment")
        mlflow.set_tags(tags)

        run_id = run.info.run_uuid
        experiment_id = run.info.experiment_id
        print(mlflow.get_artifact_uri())
        print("runID: %s" % run_id)
        mlflow.end_run()


def log_results(gridsearch, experiment_name, model_name, tags={}, log_only_best=False):
    """Logging of cross validation results to mlflow tracking server

    Args:
        experiment_name (str): experiment name
        model_name (str): Name of the model
        tags (dict): Dictionary of extra tags
        log_only_best (bool): Whether to log only the best model in the gridsearch or all the other models as well
    """
    conda_env = {
        'name': 'mlflow-env',
        'channels': ['defaults'],
        'dependencies': [
            'python=3.8.0',
            'scikit-learn>=0.21.3',
            {'pip': ['xgboost==1.0.1']}
        ]
    }

    best = gridsearch.best_index_

    mlflow.set_tracking_uri("http://0.0.0.0:5000")
    mlflow.set_experiment(experiment_name)

    if (log_only_best):
        log_run(gridsearch, experiment_name, model_name, best, conda_env, tags)
    else:
        for i in range(len(gridsearch.cv_results_['params'])):
            log_run(gridsearch, experiment_name, model_name, i, conda_env, tags)


@click.command()
@click.argument("input_data_path", type=click.Path(exists=True))
@click.argument("n_iter", type=int)
@click.argument("best", type=bool)
# @click.argument("output_model_path", type=click.Path())
# @click.argument("output_train_metrics_path", type=click.Path())
def train_model(
        input_data_path: str,
        n_iter: int,
        best: bool
        # output_model_path: str, output_train_metrics_path: str
):
    data = pd.read_csv(input_data_path)
    X = data["text"]
    y = data["target"]
    cv = StratifiedKFold(shuffle=True, random_state=42)

    cat = CatBoostClassifier(iterations=1000,
                             loss_function='Logloss',
                             eval_metric='Accuracy', od_type='Iter',
                             verbose=True)

    params = {'depth': [3, 1, 2, 6, 4, 5, 7, 8, 9, 10],
              'iterations': [250, 100, 500, 1000],
              'learning_rate': [0.03, 0.001, 0.01, 0.1, 0.2, 0.3, 0.5, 0.75, 1.0],
              'l2_leaf_reg': [3, 1, 2, 5, 10, 50],
              'border_count': [32, 20, 50, 100, 200, 350, 500],
              }
    gs_model = GS_catboost(cat, params, n_iter, cv)
    gs_model.fit(X, y)

    log_results(gs_model,
                'cat',
                'catboost',
                log_only_best=best,
                tags={})


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    train_model()
