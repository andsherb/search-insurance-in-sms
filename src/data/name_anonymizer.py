# -*- coding: utf-8 -*-


import pandas as pd
from navec import Navec
import logging
import re
import click
from slovnet import NER
import yaml
from tqdm import tqdm
import warnings
from pathlib import Path
from csv import reader, writer

warnings.filterwarnings('ignore')
tqdm.pandas()


def ner_slovnet(text, ner, navec, per=True, loc=True, org=True):
    """NER Slovnet russian"""
    changed_text = text

    ner.navec(navec)
    ners = ner(text)

    for _ in ners.spans:
        if per and _.type == 'PER':
            changed_text = changed_text.replace(text[_.start:_.stop], 'name')
        elif loc and _.type == 'LOC':
            changed_text = changed_text.replace(text[_.start:_.stop], 'place')
        elif org and _.type == 'ORG':
            changed_text = changed_text.replace(text[_.start:_.stop], 'organization')

    return changed_text


def name_extract_spacy(text, nlp, per):
    """NER Spacy multilang"""

    changed_text = text
    if per:
        doc = nlp(text)
        for _ in doc.ents:
            if _.label_ == 'PER' and \
                    re.search(r'[A-Z]([a-z]+|\.)(?:\s+[A-Z]([a-z]+|\.))*(?:\s+[a-z][a-z\-]+){0,2}\s+[A-Z]([a-z]+|\.)',
                              text[_.start_char:_.end_char]):
                changed_text = changed_text. \
                    replace(text[_.start_char:_.end_char], 'name')
        return changed_text

    else:
        return changed_text


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def anonymize_names(input_filepath: str, output_filepath:str):
    # read parameters
    with open("params.yaml", "r") as f:
        config = yaml.safe_load(f)

    per = config['ner']['per']
    loc = config['ner']['loc']
    org = config['ner']['org']

    # load cores for NER

    navec = Navec.load('models/navec_news_v1_1B_250K_300d_100q.tar')
    ner = NER.load('models/slovnet_ner_news_v1.tar')

    # read data
    # count number of rows for tqdm
    with open(input_filepath, 'r') as f:
        lines = len(f.readlines())

    with open(input_filepath, 'r') as f1, open(output_filepath, 'w') as f2:
        csv_reader = reader(f1, quotechar="'")

        csv_writer = writer(f2, delimiter=';')

        for row in tqdm(csv_reader, total=lines):
            # Slovnet NER replace Names to 'name', Locations to 'loc', Organizations to 'organization'

            try:
                row = row[0].split()
                for i in range(3):
                    row[i] = row[i].title()
            except IndexError:
                print(row)
                pass
            row = ' '.join(row)
            try:
                row = ner_slovnet(row, ner, navec, per, loc, org)
            except IndexError:
                pass

            # write row to file
            csv_writer.writerow([row])


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    anonymize_names()
