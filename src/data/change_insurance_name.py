# -*- coding: utf-8 -*-

import logging
from pathlib import Path
import re
import click
from tqdm import tqdm


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def change_names(input_filepath: str, output_filepath: str):

    with open(input_filepath, 'r') as f:
        lines = len(f.readlines())

    with open(input_filepath, 'r') as f1, open(output_filepath, 'w') as f2:
        for _ in tqdm(range(lines)):
            line = f1.readline()

            line = re.sub(r'\'\D{2,}\'', "Сбер", line)
            line = re.sub(r'\"\w+\S?\s?(\w+)?\S?\"', "Страховая ", line)
            line = re.sub(r'(^")|("$)', "'", line)
            f2.writelines(line)



if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    change_names()


