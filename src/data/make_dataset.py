# -*- coding: utf-8 -*-
import click
import logging
from pathlib import Path
import pandas as pd
import re
from tqdm import tqdm
import warnings

warnings.filterwarnings('ignore')
tqdm.pandas()


def standardize_text(text):
    """delete symbols from text"""
    text = re.sub(r'ё', 'е', text)
    text = re.sub(r'[^а-яА-Я-]', ' ', text)
    text = re.sub(r'\W*\b\w{1,3}\b', ' ', text)
    text = re.sub(r" +", " ", text)
    return text


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from clean data')

    data = pd.read_csv(input_filepath, header=None, names=['text'])
    data.text = data.text.apply(str)
    data.text = data.text.str.lower().progress_apply(standardize_text)

    data = data.dropna().drop_duplicates().reset_index(drop=True)
    idx_kasko_osago = b = data[((data.text.str.contains('осаго') != False)
                                | (data.text.str.contains('каско') != False))
                               & ((data.text.str.contains('заканч') != False)
                                  | ((data.text.str.contains('законч') != False)
                                     | (data.text.str.contains('статус') != False)
                                     | (data.text.str.contains('оформле') != False)
                                    | (data.text.str.contains('оформля') != False)
                                     | (data.text.str.contains('оформить') != False)
                                     | (data.text.str.contains('оформили') != False)
                                     | (data.text.str.contains('оплат') != False)
                                     | (data.text.str.contains('заключ') != False)
                                     | (data.text.str.contains('оплач') != False)
                                     | (data.text.str.contains('остал') != False)
                                     | (data.text.str.contains('продл') != False)
                                     | (data.text.str.contains('оконч') != False)
                                     | (data.text.str.contains('отправл') != False)
                                     | (data.text.str.contains('расчет') != False)
                                     | (data.text.str.contains('расчёт') != False)
                                     | (data.text.str.contains('активац') != False)
                                     | (data.text.str.contains('истек') != False)
                                     | (data.text.str.contains('заказ') != False)
                                     | (data.text.str.contains('заявк') != False)
                                     | (data.text.str.contains('покупк') != False)
                                     | (data.text.str.contains('концу') != False)
                                     | (data.text.str.contains('подготовл') != False)

                                     ))].index
    data['target'] = 0
    data.iloc[idx_kasko_osago, 1] = 1
    print(data.groupby('target').count())
    data.to_csv(output_filepath, index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    main()
